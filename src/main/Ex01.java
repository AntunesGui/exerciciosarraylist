package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to create a new array list, add some colors (string) and print out the collection.

public class Ex01 {
	
	private List<String> cores;
	
	public void adicionarCoresEImprimirArray() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		
		System.out.println("Cores do arrayList: " + cores);
	}

}

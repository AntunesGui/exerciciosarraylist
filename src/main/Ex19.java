package main;

import java.util.ArrayList;

//Write a Java program to trim the capacity of an array list the current list size.

public class Ex19 {

	public void verificarArrayVazio() {

		ArrayList<String> cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		System.out.println("Todos valores do Array: " + cores);
		cores.trimToSize();
		System.out.println(cores);
	}
}

package main;

import java.util.HashMap;
import java.util.Map;

public class ControllerMap {

	private Map<Integer, Integer> poly;
	private Map<Integer, Integer> poly1;
	private HashMap<Integer, Integer> result;


	public void adicionaValoresMap() {

		poly = new HashMap<Integer, Integer>();
		poly.put(1, 2);
		poly.put(2, 3);
		poly.put(5, 5);

		poly1 = new HashMap<Integer, Integer>();
		poly1.put(1, 4);
		poly1.put(2, 6);

		result = (HashMap<Integer, Integer>) poly;
	}
	public void verificaValoresMap() {
		for (Integer expo : poly1.keySet()) {
			if(result.containsKey(expo)) {
				result.put(expo, poly.get(expo) + poly1.get(expo));
			}
		}
	} 

	public HashMap<Integer,Integer> imprimirValores(){
		result.forEach((k,v) -> System.out.println(v + "x`" + k));
		return result;
	}
}

package main;

import java.util.ArrayList;

//Write a Java program to replace the second element of a ArrayList with the specified element

public class Ex21 {

	public void substituisSegundoElementoDoArray() {

		ArrayList<String> cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		System.out.println("Todos valores do Array: " + cores);
		cores.set(2, "Preto");
		System.out.println("Todos valores do Array: " + cores);
	}
}

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Write a Java program to shuffle elements in a array list.

public class Ex10 {
	
	private List<String> cores;
	
	public void embaralharElementosDoArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		System.out.println("ArrayList antes de embaralhar: " + cores);
		
		Collections.shuffle(cores);
		
		System.out.println("ArrayList apos embaralhar: " + cores);
	}
}

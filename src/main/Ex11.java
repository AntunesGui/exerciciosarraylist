package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Write a Java program to reverse elements in a array list.

public class Ex11 {

	private List<String> cores;
	
	public void inverterElementoNoArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		System.out.println("ArrayList antes de inverter posicoes: " + cores);
		
		Collections.reverse(cores);
		
		System.out.println("ArrayList antes de inverter posicoes: " + cores);
	}
}

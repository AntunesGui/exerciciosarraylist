package main;

import java.util.ArrayList;

// Write a Java program to clone an array list to another array list.

public class Ex16 {
	
	private ArrayList<String> cores = new ArrayList<String>();
	private ArrayList<String> cores1 = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public void clonarArrayListParaOutro() {

		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		System.out.println("ArrayList Original: " + cores);
		cores1 = (ArrayList<String>)cores.clone();
		System.out.println("ArrayList Clonado: " + cores1);
		
	}
}

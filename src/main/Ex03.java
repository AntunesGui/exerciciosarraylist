package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to insert an element into the array list at the first position

public class Ex03 {
	
	private List<String> cores;
	
	public void inserirElementoNoArrayNaPrimeiraPosicao() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		
		System.out.println("Cores atuais no arrayList: " + cores);
		
		cores.add(0, "Amarelo");
		
		System.out.println("Cores apos insercao na primeira posicao: " + cores);
	}
}

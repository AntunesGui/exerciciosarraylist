package main;

import java.util.ArrayList;
import java.util.List;

//Write a Java program to join two array lists. 

public class Ex15 {
	
	private List<String> cores;
	private List<String> cores1;

	public void juntarDoisArrayList() {

		cores = new ArrayList<>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		cores1 = new ArrayList<>();
		cores1.add("Azul-Marinho");
		cores1.add("Vermelho");
		cores1.add("Verde");
		cores1.add("Amarelo");
		
		cores.addAll(cores1);
		
		System.out.println("ArrayList apos Join" + cores);
	}
}

package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to iterate through all elements in a array list.

public class Ex02 {

	private List<String> cores;

	public void iterarTodosElementosDoArray() {

		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");

		for (int i = 0; i < cores.size(); i++) {
			System.out.println("Cores do array por indice: " + cores.get(i));

		}
	}
}

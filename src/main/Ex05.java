package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to update specific array element by given element.

public class Ex05 {
	
	private List<String> cores;
	
	public void atualizarElementoArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		
		System.out.println("ArrayList antes da atualizacao da cor: " + cores);
		
		cores.set(2,"Amarelo");
		
		System.out.println("ArrayList apos atualizacao da cor: " + cores);
		
	}
}

package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to remove the third element from a array list.

public class Ex06 {
	
	private List<String> cores;
	
	public void removerTerceiroElementoDoArray() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		System.out.println("Cores atuais do ArrayList sem remocao: " + cores);
		
		cores.remove(2);
		
		System.out.println("Cores do ArrayList apos remocao: " + cores);
	}
}

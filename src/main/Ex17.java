package main;

import java.util.ArrayList;
import java.util.List;

//Write a Java program to empty an array list.

public class Ex17 {
	
	private List<String> cores;

	public void limparArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		System.out.println("Array antes de apagar: " + cores);
		cores.clear();
		System.out.println("Array apagado: " + cores);

	}
}

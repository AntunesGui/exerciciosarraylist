package main;

import java.util.ArrayList;
import java.util.List;

//Write a Java program to extract a portion of a array list. 

public class Ex12 {

	private List<String> cores;
	
	public void extrairPorcaoDoArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		System.out.println("ArrayList antes de extrair particoes: " + cores);
		
		List<String> subCores = cores.subList(0, 2);
		
		System.out.println("List da particao extraida: " + subCores);
	}
}

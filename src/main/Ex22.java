package main;

import java.util.ArrayList;

//Write a Java program to print all the elements of a ArrayList using the position of the elements

public class Ex22 {
	int index = 0;
	public void imprimirTodosElementosComSuasPsocicoesNoArray() {

		ArrayList<String> cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		for (int i = 0; i < cores.size(); i++) {
			System.out.println(cores.get(i) + "="+ i);
		}
		for (String arrCores : cores) {

			System.out.println(arrCores + "=" + index);
			index ++;
		}
	}
}

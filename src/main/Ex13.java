package main;

import java.util.ArrayList;
import java.util.List;

//Write a Java program to compare two array lists.

public class Ex13 {

	private List<String> cores;
	private List<String> cores1;

	public void compararDoisArrayListsComIf() {

		cores = new ArrayList<>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");

		cores1 = new ArrayList<>();
		cores1.add("Azul");
		cores1.add("Vermelho");
		cores1.add("Verde");
		cores1.add("Amarelo");

		if(cores.equals(cores1)) {
			System.out.println("Arrays iguais!");
		} else {
			System.out.println("Arrays diferentes!");
		}
	}

	public void compararDoisArrayListsComFor() {

		cores = new ArrayList<>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");

		cores1 = new ArrayList<>();
		cores1.add("Azul");
		cores1.add("Vermelho");
		cores1.add("Verde");
		cores1.add("Amarelo0");

		ArrayList<String> cores2 = new ArrayList<>();
		for (String c0 : cores) {
			if(cores1.contains(c0)) {
				cores2.add(c0);
			}
		}
		System.out.println("Cores iguais: " + cores2);
	}

	public void compararDoisArrayListsComFor1() {

		cores = new ArrayList<>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");

		cores1 = new ArrayList<>();
		cores1.add("Azul");
		cores1.add("Vermelho");
		cores1.add("Verde");
		cores1.add("Amarelo0");

		ArrayList<String> cores2 = new ArrayList<>();
		for (int i = 0; i < cores.size(); i++) {
			for (int j = 0; j < cores1.size(); j++) {
				if(cores.get(i).equals(cores1.get(j))) {
					cores2.add(cores.get(i));
				}
			}
		}
		System.out.println("Cores iguais: " + cores2);
	}
}

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Write a Java program of swap two elements in an array list.

public class Ex14 {

	private List<String> cores;

	public void trocarElementosDoArrayListComSwap() {

		cores = new ArrayList<>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");

		System.out.println("ArrayList antes de trocar elementos: " + cores);

		Collections.swap(cores, 0, 2);

		System.out.println("ArrayList depois de trocar elementos: " + cores);
	}

	public void trocarElementosDoArrayList() {

		String[] arr = {"Azul", "Vermelho", "Verde", "Amarelo"};
		int indexZero = 0;
		int indexDois = 2;

		ArrayList<String> arrList = new ArrayList<>();
		for (int i = 0; i < arr.length; i++) {
			arrList.add(arr[i]);
		}
		System.out.println("ArrayList antes de trocar elementos: " + arrList);
		String cor = arr[indexZero];
		arr[indexZero] = arr[indexDois];
		arr[indexDois] = cor;
		for (String cores : arr) {
			System.out.println("ArrayList depois de trocar elementos: " + cores);
		}
	}
}

package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to retrieve an element (at a specified index) from a given array list.

public class Ex04 {

	private List<String> cores;
	
	public void recuperarElementoDeUmArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		
		System.out.println("Elemento recuperado de um determinado index: " + cores.get(2));
	}
}

package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to test an array list is empty or not.

public class Ex18 {
	
	private List<String> cores;
	
	public void verificarArrayVazio() {
		
		cores = new ArrayList<>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		if(cores.isEmpty()) {
			System.out.println("Array vazio " + cores);
		} else {
			System.out.println("Array preenchido: " + cores);
		}
	}
}

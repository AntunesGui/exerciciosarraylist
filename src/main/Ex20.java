package main;

import java.util.ArrayList;

//Write a Java program to increase the size of an array list.

public class Ex20 {
	
	public void aumentarArray() {

		ArrayList<String> cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		System.out.println("Todos valores do Array: " + cores.size());
		cores.ensureCapacity(6);
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		System.out.println(cores.size());
	}

}

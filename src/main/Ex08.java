package main;
// Write a Java program to sort a given array list.

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Ex08 {

	private List<String> cores;
	
	public void odernarArrayList() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");

		System.out.println("ArrayList nao odernado: " + cores);
		
		Collections.sort(cores);
		
		System.out.println("ArrayList odernado: " + cores);
	}
}

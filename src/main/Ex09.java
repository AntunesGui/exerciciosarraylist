package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Write a Java program to copy one array list into another.

public class Ex09 {
	
	private List<String> coresOld;
	private List<String> coresNew;
	
	public void copiarArrayListParaOutroComFor() {
		
		coresOld = new ArrayList<String>();
		coresNew = new ArrayList<String>();
		
		coresOld.add("Azul");
		coresOld.add("Vermelho");
		coresOld.add("Verde");
		coresOld.add("Amarelo");
		
		coresNew.add("Caramelo");
		coresNew.add("Roxo");
		coresNew.add("Preto");
		coresNew.add("Turquesa");

		System.out.println("Cores atuais do ArrayNew: \n" + coresNew);
		coresNew.clear();
		
		for (String cores : coresOld) {
			coresNew.add(cores);
			System.out.println("Adicionado cor do ArrayOld para ArrayNew: \n" + cores);
		}
		System.out.println("Todas as cores novas do ArrayNew: \n" + coresNew);
	}
	
	public void copiarArrayListParaOutroComCopy() {
		
		coresOld = new ArrayList<String>();
		coresNew = new ArrayList<String>();
		
		coresOld.add("Azul");
		coresOld.add("Vermelho");
		coresOld.add("Verde");
		coresOld.add("Amarelo");
		
		coresNew.add("Caramelo");
		coresNew.add("Roxo");
		coresNew.add("Preto");
		coresNew.add("Turquesa");
		
		System.out.println("Cores atuais do ArrayNew: \n" + coresNew);
		
		Collections.copy(coresNew, coresOld);
		
		System.out.println("Todas as cores novas do ArrayNew: \n" + coresNew);
	}
}

package main;

import java.util.ArrayList;
import java.util.List;

// Write a Java program to search an element in a array list.

public class Ex07 {

	private List<String> cores;

	public void procurarElementoNoArrayListComFor() {

		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		for (String arrayCores : cores) {
			if(arrayCores.equals("Vermelho")) {
				System.out.println("Achei a cor: " + arrayCores);
				break;
			} else {
				System.out.println("Nao achei a cor: " + arrayCores);
			}
		}
	}
	
	public void procurarElementoNoArrayListSemFor() {
		
		cores = new ArrayList<String>();
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Verde");
		cores.add("Amarelo");
		
		if(cores.contains("Vermelho")) {
			System.out.println("Achei a cor procurada");
		}
	}
}
